/// <reference types="cypress" />

describe('Home page', function () {
    before('Open app', function () {
      cy.visit('/')
    })
  
    it('loads stories', function () {
      cy.get('.story:nth-child(1) > .stories-content > h3', {
        timeout: 10000
      }).contains('This is a test story')
    })
  
    it('does not allow user to vote', function () {
      cy.get('.votes-count')
        .invoke('text')
        .then((val1) => {
          cy.get('.vote-button').click({
            timeout: 10000
          })
  
          cy.get('.votes-count')
            .invoke('text')
            .then((val2) => {
              expect(val1).to.equal(val2)
            })
        })
    })
  
    it('links to story author', function () {
      cy.get('[data-cy=author-link').should(
        'have.attr',
        'href',
        '/profile/605a30f6a4aa16137c4254a5'
      )
    })
  
    it('loads stories in different sections', function () {
      const buttonCount = 6
      for (let i = 2; i <= buttonCount; i++) {
        cy.get(`.roadmap > button:nth-child(${i})`).click({
          timeout: 10000
        })
        cy.contains('No stories')
      }
    })
  
    it('should open stories page when user clicks on a story', function () {
      cy.visit('/', {
        timeout: 10000
      })
      cy.get('.story:nth-child(1) > .stories-content')
        .click()
        .click()
        .url()
        .should('equal', 'http://localhost:3000/story/605a35e2a4aa16137c4254a7')
    })
})
  