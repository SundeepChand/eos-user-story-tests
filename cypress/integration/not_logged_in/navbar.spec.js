/// <reference types="cypress" />

describe('Navbar', function () {
    before('Open app', function () {
      cy.visit('/')
    })
  
    it('contains EOS logo', function () {
      cy.get('[data-cy=logo]')
        .should('have.attr', 'href', '/')
        .should('contain.html', 'img')
    })
  
    it('contains Sign In button', function () {
      cy.get('[data-cy=sign-in]')
        .should('have.attr', 'href', '/login')
        .contains('Sign In')
    })
})
  